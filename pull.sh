#!/usr/bin/env bash
CURDIR=$(pwd)
for name in $(find ${CURDIR}/src -name ".git")
do
	git_proj_dir=$(dirname $name)
	cd $git_proj_dir
	pwd
	git pull origin master
	#git config credential.helper store
	#git branch
done

