#!/usr/bin/env bash

UNAME="jiangxumin"
PASSWD="passwd"
REPOS_FILE="master.repos"

sed -i  "s/USER_NAME/${UNAME}/"  ${REPOS_FILE}
sed -i  "s/PASSWD/${PASSWD}/"  ${REPOS_FILE}

mkdir -p src
vcs import src <  ${REPOS_FILE}

find ./src -name "config" | grep ".git"  |xargs sed -i "s/${UNAME}:${PASSWD}@//g"

#cat ./master.repos

sed -i  "s/${UNAME}/USER_NAME/"  ${REPOS_FILE}
sed -i  "s/${PASSWD}/PASSWD/"  ${REPOS_FILE}

git checkout  vcs.sh
